var Record = require('./Record');
var Value = require('./Value');
var Log = require('./Log');
var inc = 0;

class Condition {
	constructor(mode, conditions){
		this.id = ++inc;
		this.type = undefined;
		this.mode = mode;
		this.conditions = (typeof conditions == 'string' ||
						   conditions.length === undefined) ? [conditions] : conditions;
	}

	test(record) {
		Log.log(this.id + ' ==============================');
		Log.log(this);

		var gret;

		for(var i=0, ret; i<this.conditions.length; i++) {
			if(this.conditions[i] instanceof Condition) {
				this.conditions[i].type = this.type;
				ret = this.conditions[i].test(record)
			}
			else {
                ret = Condition.testInternal(this.type, record, this.conditions[i]);
            }

			Log.log('\n checking: mode('+this.mode+') ret('+ret+')');

			if(this.mode == 'not') {
				Log.log('not, returning');
				gret = !ret;
				break;
			}

			if(this.mode == 'single') {
				Log.log('single, returning');
				gret = ret;
				break;
			}

			if(this.mode == 'and' && ret == false) {
				Log.log('and, returning');
				gret = false;
				break;
			}

			if(this.mode == 'or' && ret == true) {
				Log.log('or, returning');
				gret = true;
				break;
			}
		}

		var ret = gret;
		if(ret == undefined) ret = this.mode == 'and';

		Log.log('\nreturning ', ret);
		Log.log(this.id + ' ==============================');
		return ret;
	}


    static testInternal(type, record, condition) {
        Log.log('\n    > Query.testCondition "'+type+'" ', condition);
        var ret;
        if(type == 'datafield') {
            ret = record.hasDatafield(condition);
            if(ret) record.matches.push({ tag : condition.tag, code : condition.code, value : ret })
        }
        else if(type == 'controlfield') {
            ret = record.hasControlfield(condition);
        }
        else if(type == 'type') {
			ret = Value.test(condition, record.type);
        }

        Log.log('    > result: ', ret);
        return ret != false;
    }
}


module.exports = Condition;
