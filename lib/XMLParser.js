var Record 		= require('./Record');
const fs        = require('fs');
const zlib      = require('zlib');
const readline  = require('readline');

const contentReg 		= new RegExp(/>([^<]*)</);
const attributesReg 	= new RegExp(/(\S+)=["'](.*?)["']/g);
const tagRegExp 		= new RegExp(/<(\w+)\s?([^>]*|)>([^<]*)(<\/?\w+>)?/g);
const closingTagRegExp 	= new RegExp(/<\/(\w+)>/);

const tags = {};
tags.RECORD  = 'record';
tags.LEADER  = 'leader';
tags.CONTROL = 'controlfield';
tags.DATA 	 = 'datafield';
tags.SUB 	 = 'subfield';


class XMLParser {
	constructor(options) {
		this.saveRawXML = options.saveRawXML || false;
		this.gzip = options.gzip;
		this.encoding = options.encoding;
		this.lineReader = undefined;
		this.currentRecord = undefined;
		this.currentDataField = undefined;
		this.currentRecordXML = '';
		this.callbacks = {};
		this.stopping = false;
        this.numParsed = 0;
        this.offset = 0;
	}

	read(file, offset) {
        if(offset) this.offset = offset;
		var input = fs.createReadStream(file, { encoding : this.encoding});

		if(this.gzip !== false) {
			if(this.gzip === true || file.indexOf('.gz') != -1) input = input.pipe(zlib.createGunzip());
		}

		this.lineReader = readline.createInterface({ input: input });
		this.lineReader.on('line', (line) => {
			if(this.stopping) return;
			this.parse(line);
		});
		this.lineReader.on('close', () => {
			if(typeof this.callbacks['close'] == 'function') this.callbacks['close']();
		})
	}

	stop(){
		this.stopping = true;
		this.lineReader.close();
	}

	parse(line) {
        var recordOnly = false;
        if(this.numParsed < this.offset) recordOnly = true; // only check if line is open/closed record tag -> faster

		var parsed = XMLParser.parseLine(line, recordOnly);

		if(parsed.tag == tags.RECORD) {
            if(parsed.open && this.numParsed < this.offset) {
                this.currentRecord = undefined;
                return;
            }

			if(this.saveRawXML) this.currentRecordXML += line + '\n';
			if(parsed.closed) {
                this.numParsed++;
				if(this.currentRecord && typeof this.callbacks['record'] == 'function') this.callbacks['record'](new Record(this.currentRecord, this.currentRecordXML));
				return;
			}

			this.currentRecord = { type : parsed.attributes.type };
			if(this.saveRawXML) this.currentRecordXML = '';
			if(this.saveRawXML) this.currentRecordXML += line + '\n';
			return;
		}

        if(this.currentRecord == undefined) return; //skipping the current record

		if(parsed.tag == tags.LEADER) {
			if(this.saveRawXML) this.currentRecordXML += line + '\n';
			this.currentRecord.leader = parsed.content;
			return;
		}

		if(parsed.tag == tags.CONTROL) {
			if(this.saveRawXML) this.currentRecordXML += line + '\n';
			if(!this.currentRecord.controlfields) this.currentRecord.controlfields = [];
			this.currentRecord.controlfields.push({
				tag : parsed.attributes.tag,
				value : parsed.content
			})
			return;
		}

		if(parsed.tag == tags.DATA) {
			if(this.saveRawXML) this.currentRecordXML += line + '\n';
			if(parsed.closed) return;

			if(!this.currentRecord.datafields) this.currentRecord.datafields = [];
			var field = {
				tag : parsed.attributes.tag,
				ind1 : parsed.attributes.ind1,
				ind2 : parsed.attributes.ind2
			};
			this.currentDataField = field;
			this.currentRecord.datafields.push(field);
			return;
		}

		if(parsed.tag == tags.SUB) {
			if(this.saveRawXML) this.currentRecordXML += line + '\n';
			if(!this.currentDataField.subfields) this.currentDataField.subfields = [];
			this.currentDataField.subfields.push({
				code : parsed.attributes.code,
				value : parsed.content
			});
			return;
		}
	}

	on(event, callback) {
		this.callbacks[event] = callback;
	}

	static parseLine(string, recordOnly) {
		var parsed = { tag : '', attributes : {}, content : '', open : false, closed : false };

        if(recordOnly === true) {
            if(string.indexOf('<record') != -1) {
                parsed.tag = 'record';
                parsed.open = true;
            }
            else if(string.indexOf('</record') != -1) {
                parsed.tag = 'record';
                parsed.closed = true;
            }

            return parsed;
        }

        var m;

		while ((m = tagRegExp.exec(string)) !== null) {
		    if (m.index === tagRegExp.lastIndex)  tagRegExp.lastIndex++;
			parsed.tag = m[1];
			parsed.attributes = m[2];
			parsed.content = m[3];
			parsed.closed = m[4] != undefined;
			parsed.open = true;
		}

		if(parsed.tag != '' && parsed.attributes) {
			parsed.attributes = XMLParser.parseAttributes(parsed.attributes);
		}

		if( parsed.tag == '' && (m = closingTagRegExp.exec(string)) !== null) {
			parsed.tag = m[1];
			parsed.open = false;
			parsed.closed = true;
		}

		return parsed;
	}

	static parseAttributes(string){
		var m;
		var attributes = {};
		while ((m = attributesReg.exec(string)) !== null) {
			if (m.index === attributesReg.lastIndex) attributesReg.lastIndex++;
			attributes[m[1]] = m[2];
		}

		return attributes;
	}
}


module.exports = XMLParser;
