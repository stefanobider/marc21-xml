var XMLParser = require('./XMLParser');
var Query = require('./Query');
var fn = require('./fn');
var fs = require('fs');
var doLog = false;
const readline = require('readline');

var log = function() {
	var string = arguments[0];
	if(arguments.length > 1) string = Array.prototype.slice.call(arguments).join(' ');

	readline.clearLine(process.stdout);
	readline.cursorTo(process.stdout, 0);
	process.stdout.write(string);
}

/*
options.in {array | string}
	List of files to parse

options.gzip {boolean}
	Whether the file is gzipped. Default: inferred from file extension.

options.search {number}
	Stop after 'search' records have been analyzed, independently on how many
	were fetched. Default : (no limit)

options.skip {number}
	Skip the first 'skip' records without evaluating them. Default : 0

options.log {number | boolean}
	If number, the frequency of logging (e.g. log every 2000 records). If false,
	do not log. Default: 10

options.out {string}
	Path to file where to save results, without file extension (e.g. 'output' will
	save to out.json, out.xml, out.txt)

options.outFormat {array | string}
	Format(s) of output file, available: 'json', 'xml', 'human' (txt). Default: 'json'

options.rotate {number | boolean}
	If number, how many records per output file. If false, do not rotate. Default: 500

options.memory {bool}
	Whether to save in memory the results. If true, the array of results is
	returned on completions. If false, only the number of results is returned.
	Default : true if no output file set, false otherwise.

options.rawRecords {bool}
	Whether the record returned on 'record' and saved in memory should be raw json
	object or internal Record objects. Default: false
*/
class Parser {
	constructor(options) {
		this.in = options.in;
		this.encoding = options.encoding;
		this.out = options.out;
		this.search = options.search;
		this.skip = options.skip;
		this.rotate = (options.rotate === false) ? Infinity : options.rotate || 500;
		this.log = (options.log === false) ? 0 : options.log;
		this.callbackFrequency = options.callbackFrequency || 1;

		this.userRequestedMemory = false;

		if(options.memory !== undefined) {
			this.memory = options.memory;
			if(this.memory) this.userRequestedMemory = true;
		}
        else if(this.out) this.memory = false;
		else this.memory = true;

		this.outFormat = options.outFormat || ['json'];
		this.rawRecords = options.rawRecords || false;
		this.gzip = options.gzip || undefined;

		this.q = undefined;
		this.parser = undefined;
        this.callbacks = {};

		this.limit = Infinity;
		this.offset = 0;
		this.idOffset = false;
		this.evaluateId = false;
		this.outInitialized = false;
		this.results = [];
		this.callbackData = undefined;
		this.aborted = false;
		this.parsed = 0;
		this.matched = 0;
		this.fetched = 0;
		this.currentPath = undefined;
		this.written = 0;
		this.currentOutFileIndex = 0;
		this.stopping = false;
        this.saveRawXML = false;

        this.startTime = undefined;
        this.endTime = undefined;

		if(this.out) this.openOutFile();
	}

    on(event, callback) {
        if(event == 'records') event = 'record';
        this.callbacks[event] = callback;

		if(event == 'record' && this.memory === true && !this.userRequestedMemory) {
			this.memory = false;
		}
	}

	getInFile() {
		if(typeof this.in == 'string') this.in = [this.in];
		if(this.in.length == 0) return null;
		return this.in.splice(0,1)[0];
	}

	openOutFile() {
		if(!Array.isArray(this.outFormat)) this.outFormat = [this.outFormat];

		var baseName = './' + this.out;
		if(this.currentOutFileIndex) baseName += '.' + this.currentOutFileIndex;

		if(this.outFormat.indexOf('json') != -1) {
			if(fs.existsSync(baseName+'.json')) fs.unlinkSync(baseName+'.json');
			this.outStreamJson = fs.createWriteStream(baseName+'.json', { flags: 'a' });
            this.outStreamJson.write('[');
		}

		if(this.outFormat.indexOf('xml') != -1) {
			if(fs.existsSync(baseName+'.xml')) fs.unlinkSync(baseName+'.xml');
            this.saveRawXML = true;
			this.outStreamXml = fs.createWriteStream(baseName+'.xml', { flags: 'a' });
			this.outStreamXml.write('<?xml version="1.0" encoding="UTF-8"?><collection xmlns="http://www.loc.gov/MARC21/slim">\n');
		}

		if(this.outFormat.indexOf('human') != -1) {
			if(fs.existsSync(baseName+'.txt')) fs.unlinkSync(baseName+'.txt');
			this.outStreamHuman = fs.createWriteStream(baseName+'.txt', { flags: 'a' });
		}

		this.outInitialized = true;
		this.currentOutFileIndex++;
		this.written = 0;
	}

	closeOutFile() {
		if(!this.outInitialized) return;
		this.outInitialized = false;

		if(this.outStreamJson) {
			this.outStreamJson.write(']');
			this.outStreamJson.end();
			this.outStreamJson = undefined;
		}

		if(this.outStreamXml){
			this.outStreamXml.write('</collection>');
			this.outStreamXml.end();
			this.outStreamXml = undefined;
		}

		if(this.outStreamHuman) {
			this.outStreamHuman.end();
			this.outStreamHuman = undefined;
		}

	}

	writeToOut(record) {
		if(this.out && !this.outInitialized) this.openOutFile();

		if(this.outStreamJson) {
			var jstring = record.JSON();
			if(this.written > 0) jstring = ',' + jstring;
			this.outStreamJson.write(jstring);
		}

		if(this.outStreamXml) {
			this.outStreamXml.write(record.XML());
		}

		if(this.outStreamHuman) {
			this.outStreamHuman.write(record.print());
			this.outStreamHuman.write('\n\n ============================================ \n\n')
		}

		this.written++;

		if(this.written >= this.rotate)  {
			this.closeOutFile();
		}
	}

	onEnd() {
		this.closeOutFile();

		if(!this.aborted && this.callbackData && typeof this.callbacks.record == 'function') {
			this.callbacks.record(this.callbackData);
		}

		if(!this.aborted && typeof this.callbacks.end == 'function') {
			this.callbacks.end(this.results);
		}

		if(this.aborted && typeof this.callbacks.error == 'function') {
			this.callbacks.error();
		}
	}

	onRecord (record) {
		if(this.aborted) return;
		this.parsed++;

		if(this.log) doLog = this.parsed%this.log == 0;

        if(this.callbacks.transform) this.callbacks.transform(record);

		if(this.q.test(record)) this.onMatch(record);

		if(doLog && this.log !== false) {
			log("file: '"+this.currentPath+"', parsed: "+this.parsed+", matched: " + this.matched);
		}

		if( (this.limit && this.fetched >= this.limit) ||
			(this.search && this.parsed >= this.search) )
		{
			this.stopping = true;
			return this.parser.stop();
		}
	}

	onMatch(record) {
		this.matched++;

		if(this.matched <= this.offset) return;

		this.fetched++;
		record.set(this.q.filter(record));
		if(this.memory) {
			if(this.rawRecords) this.results.push(record.get());
			else this.results.push(record);
		}
		this.writeToOut(record);

		if(this.log) doLog = true;

		if(typeof this.callbacks.record != 'function') return;

		var cbRecord = this.rawRecords ? record.get() : record;

		if(this.callbackFrequency > 1) {
			if(this.callbackData === undefined) this.callbackData = [];
			this.callbackData.push(cbRecord);
			if(this.callbackData.length < this.callbackFrequency) return;
		}
		else {
			this.callbackData = cbRecord;
		}

		if(this.callbacks.record(this.callbackData) === false){
			this.aborted = true;
			this.stopping = true;
			this.parser.stop();
		}
		this.callbackData = undefined;
	}

	start(path) {
		this.currentPath = path;
		this.parser = new XMLParser({ saveRawXML : this.saveRawXML, gzip : this.gzip, encoding: this.encoding });

		this.parser.on('record', this.onRecord.bind(this));

		this.parser.on('close', () => {
            this.endTime = process.hrtime();
            this.totalTime = (this.endTime[0] + this.endTime[0]/1e9) - (this.startTime[0] + this.startTime[0]/1e9)

			if(this.log) {
				console.log('\n\n');
				console.log('Done with "'+this.currentPath+'". Parsed ' + this.parsed + ' records, of which ' + this.fetched + ' were fetched. Search took ' + this.totalTime + 's');
				console.log('\n\n');
			}

			var path = this.getInFile();
			if(path === null || this.stopping) return this.onEnd();
			this.start(path);
		})

        this.startTime = process.hrtime();
		this.parser.read(path, this.skip);
	}

	findById() {
		var query = { where : { controlfield : { tag : '001' , value : fn.EQUALS.apply(null, arguments) } }};
		query.limit = arguments.length;
		return this.find(query);
	}

	findOne(query) {
		query.limit = 1;
		return this.find(query);
	}

	findText() {
		var query = { where : { datafield : { value : fn.CONTAINS.apply(null, arguments) } }};
		return this.find(query);
	}

	find(query) {
		this.matched = 0;
		this.parsed = 0;
		this.q = new Query(query);
		this.limit = this.q.limit;
		this.offset = this.q.offset;
		if(typeof this.offset == 'string') {
			this.idOffset = true;
			this.evaluateId = true;
		}

        var p = new Promise((resolve, reject) => {
			this.callbacks.end = resolve;
			this.callbacks.error = reject;
			this.start(this.getInFile());
		});

        p.transform = (callback) => { this.on('transform', callback); return p; }
        p.each = (callback) => { this.on('record', callback); return p; }
        return p;
	}

}

module.exports = Parser;
