class Value {
	constructor(fn, mode, strings){
		this.mode = mode || 'or';
		this.fn = fn;
		this.strings = strings;
		if(typeof this.strings == 'string') this.strings = [strings];
	}

	testInternal(value) {
		for(var i=0, ret; i<this.strings.length; i++) {
			ret = Value[this.fn](value, this.strings[i]);
			if(this.mode == 'and' && ret == false) return false;
			if(this.mode == 'or'  && ret == true) return this.strings[i];
		}

        if(this.mode == 'and') return this.strings;
		else return false;
	}

	static test(source, value) {
		if(value instanceof Value) return value.testInternal(source);
		return Value.equals(source, value) ? value : false;
	}

	static equals(source, value)   { return source == value }
	static equalsi(source, value)  { return source.toLowerCase() == value.toLowerCase() }
	static nequals(source, value)  { return source != value }
	static contains(source, value) { return source.toLowerCase().indexOf(value.toLowerCase()) != -1 }
	static ncontains(source, value){ return source.toLowerCase().indexOf(value.toLowerCase()) == -1 }
}

module.exports = Value;
