var Value = require('./Value');
var Log = require('./Log');

class Record {
	constructor(record, xml) {
		this.internal = record;
		this.type = this.internal.type;
		this.leader = this.internal.leader;
		this.xml = xml;
        this.matches = [];
	}

	print() {
		var p = '';
		if(this.internal.type) p += 'type   : ' + this.internal.type + '\n';
		if(this.internal.leader) p += 'leader : ' + this.internal.leader + '\n';
		if(this.internal.controlfields) {
			p += 'controlfields\n';

			for(var i=0; i<this.internal.controlfields.length; i++) {
				p += '	tag "' + this.internal.controlfields[i].tag + '" : ' + this.internal.controlfields[i].value + '\n';
			}
		}


		if(this.internal.datafields) {
			p += 'datafields\n';

			for(var i=0, j; i<this.internal.datafields.length; i++) {
				for(j=0; j<this.internal.datafields[i].subfields.length; j++) {
					p += '	tag "' + this.internal.datafields[i].tag + '" ind1 "' + this.internal.datafields[i].ind1 + '"  ind2 "' + this.internal.datafields[i].ind2 + '" ';
					p += ' code "' + this.internal.datafields[i].subfields[j].code + '" : ' + this.internal.datafields[i].subfields[j].value + '\n';
				}
				p += '\n';
			}
		}
		return p;
	}

	get() { return this.internal; }
	set(record) { this.internal = record; }
	JSON() { return JSON.stringify(this.internal); }
	XML() { return this.xml; }

	id() {
		if(!this.internal ||
			!this.internal.controlfields || 
			!this.internal.controlfields.length) return '';

		for(var i=0; i<this.internal.controlfields.length; i++) {
			if(this.internal.controlfields[i].tag == '001') return this.internal.controlfields[i].value;
		}

		return '';
	}

	/*
	Checks if record has at least one controlfield that matches the selector
	*/
	hasControlfield(selector) {
		for(var i=0, controlfield; i<this.internal.controlfields.length; i++) {
			controlfield = this.internal.controlfields[i];
			if(controlfield.tag == selector.tag &&
				(!selector.value || (selector.value && Value.test(controlfield.value, selector.value)) ) )
			{
				return true;
			}
		}
		return false;
	}

	/*
	Checks if record has at least one datafield that matches the selector
	*/
	hasDatafield(selector) {
		for(var i=0, matchedValue; i<this.internal.datafields.length; i++) {
			if( (matchedValue = Record.checkDatafield(this.internal.datafields[i], selector.tag, selector.ind1, selector.ind2, selector.code, selector.value)) ){
				//this.internal.matchedValue = matchedValue;
				return matchedValue;
			}
		}
		return false;
	}

	/*
	Returns an array containing all datafields matchind the selector
	*/
	datafields(selector) {
		if(!selector) return this.internal.datafields;
		var ret = [];
		for(var i=0, datafield; i<this.internal.datafields.length; i++) {
			datafield = this.internal.datafields[i];
			if(Record.checkDatafield(datafield, selector.tag, selector.ind1, selector.ind2, selector.code, selector.value)) {
				if(selector.full === false) datafield.subfields = Record.getSubfields(datafield, selector.code, selector.value);
				ret.push(datafield);
			}
		}
		return ret;
	}

    /*
    Calls String.replace on every value of every subfiled and controlfield
    */
    replace() {
        var args = arguments;
        this.internal.datafields.forEach(d => {
            d.subfields.forEach(sub => {
                sub.value = sub.value.replace.apply(null, args);
            })
        })

        this.internal.controlfields.forEach(c => {
            c.value = c.value.replace.apply(null, args);
        })
    }

	static getSubfields(datafield, code, value) {
		var ret = [];
		for(var i=0, subfield; i<datafield.subfields.length; i++){
			subfield = datafield.subfields[i];

			if(subfield.code != code) continue;
			if(value && !Value.test(subfield.value, value)) continue;
			ret.push(subfield);
		}
		return ret;
	}

	static hasSubfield(datafield, code, value) {
		for(var i=0, subfield; i<datafield.subfields.length; i++){
			subfield = datafield.subfields[i];

			if(subfield.code != code) continue;
			if(value && !Value.test(subfield.value, value)) continue;
			return true;
		}

		return false;
	}

	/*
	Tests a datafield against the paramenters
	*/
	static checkDatafield(datafield, tag, ind1, ind2, code, value) {
		if(tag && datafield.tag != tag) {
			Log.log('different tag ('+datafield.tag+' / ' +tag+')');
			return false;
		}

		if(ind1 && datafield.ind1 != ind1) {
			 Log.log('different ind1 ('+datafield.ind1+' / ' +ind1+')');
			return false;
		}

		if(ind2 && datafield.ind2 != ind2) {
			 Log.log('different ind2 ('+datafield.ind2+' / ' +ind2+')');
			return false;
		}

		if(!code && !value) {
			 Log.log('no code nor value requested');
			return true;
		}

		for(var i=0, subfield, matchedValue; i<datafield.subfields.length; i++){
			subfield = datafield.subfields[i];

			if(code) {
				if(subfield.code != code) continue;
				if(!value) return true;
			}

			if(value && (matchedValue = Value.test(subfield.value, value)) ) return matchedValue;
		}

		return false;
	}
}

module.exports = Record;
