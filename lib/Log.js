var enabled = false;

var Log = {};
Log.log = function(){
    if(!enabled) return;
    console.log.apply(console.log, arguments);
}

Log.enable = function(enable) {
    enabled = enable;
}

module.exports = Log;
