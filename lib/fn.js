const Condition = require('./Condition');
const Value = require('./Value');


module.exports = {
    SINGLE    	 : function(){ return new Condition('single', arguments[0]) },
    AND          : function(){ return new Condition('and',  (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    OR           : function(){ return new Condition('or', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    NOT       	 : function(){ return new Condition('not', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    EQUALS    	 : function(){ return new Value('equals', 'or', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    NEQUALS    	 : function(){ return new Value('nequals', 'and', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    CONTAINS  	 : function(){ return new Value('contains', 'or', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    NCONTAINS  	 : function(){ return new Value('ncontains', 'and', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
    CONTAINS_ALL : function(){ return new Value('contains', 'and', (arguments.length == 1) ? arguments[0] : Array.prototype.slice.call(arguments)) },
};
