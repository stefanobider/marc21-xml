var Record = require('./Record');
var Value = require('./Value');
var Condition = require('./Condition');
var fn = require('./fn');
var Log = require('./Log');

/*
options.limit {number}
	How many results to fetch. Default: Infinity.

options.offset {number}
	Number of results to discard, starting from 0. Default: 0.

options.where {undefined | object}


options.select {undefined | '*' | object}
	If undefined or '*', all fields are retrieved. If object, the following sub-options
	are available:

	options.select.leader {boolean}
		Whether to retrieve the leader. default: false.

	options.select.type {boolean}
		Whether to retrieve the type. default : false.

	options.select.datafields {boolean | array | object}
		If true, all datafields are retrieved. If object, the datafields can be selected
		with the following object:
			{ tag : '079', code : 'u', value : 'xyt', full : true}
		The object retrieves only the datafields '079' that contain subfield(s) of code 'u'
		and value 'xyt'. The 'full' parameter tells whether only the matching subfield(s)
		should be retrieved or the whole matching datafield.
		It is possible to pass an array of selections objects to select multiple
		datafields.

	options.select.controlfields {boolean | array | object}
		Same rules as for datafields apply, the selection object is defined as:
			{ tag : '001', value : 'xyt'}
*/

class Query {
	constructor(query) {
		if(!query) query = { select : '*' };

		this.query = query;

		this.select = query.select;
		this.where  = query.where;
		this.limit  = query.limit || Infinity;
		this.offset = query.offset || 0;

		if(!this.where && !this.select) this.where = query;

		if(this.select) this.prepareSelect();
		if(this.where) this.prepareWhere();
	}

	/*
	Test a record against the query
	*/
	test(record) {
		Log.log('\n\n RECORD \n\n');

		if(!this.where) {
			Log.log(' No where condition, returning true');
			return true;
		}

		var typeCheck = undefined;
		var controlfieldCheck = undefined;
		var datafieldCheck = undefined;

		var ret = undefined;

		if(this.where.controlfield) {
			controlfieldCheck = this.where.controlfield.test(record);
			ret = controlfieldCheck;
		}

		if(this.where.datafield) {
			datafieldCheck = this.where.datafield.test(record);
			if(ret === undefined) ret = datafieldCheck;
			else ret = datafieldCheck || ret;
		}

		if(this.where.type) {
			typeCheck = this.where.type.test(record);
			if(ret === undefined) ret = typeCheck;
			else ret = typeCheck && ret;
		}


		Log.log('\n\n final decision : ', controlfieldCheck, typeCheck,  datafieldCheck, ' > ', ret);
		return ret;
	}

	filter(record) {
		if(!this.select) return record.get();

		var r = { };

		if(this.select.datafields === true) {
			r.datafields = record.get().datafields;
		}
		else if(typeof this.select.datafields == 'object') {
			r.datafields = [];
			for(var i=0, res, d; i<this.select.datafields.length; i++) {
				r.datafields = r.datafields.concat(record.datafields(this.select.datafields[i]));
			}
		}

		if(this.select.controlfields === true) {
			r.controlfields = record.get().controlfields;
		}

		return r;
	}

	prepareSelect() {
		if(this.select === '*') {
			this.select = undefined;
			return;
		}

		if(!this.select.leader) this.select.leader = false;

		if(this.select.controlfield) {
			this.select.controlfields = this.select.controlfield;
			delete this.select.controlfield;
		}

		if(!this.select.controlfields) this.select.controlfields = false;

		if(this.select.datafield) {
			this.select.datafields = this.select.datafield;
			delete this.select.datafield;
		}

		if(!this.select.datafields) this.select.datafields = false;

		if(typeof this.select.datafields == 'object' && this.select.datafields.length == undefined) {
			this.select.datafields = [this.select.datafields];
		}
	}

	prepareWhere() {
		for(var x in this.where) {
			if( !(this.where[x] instanceof Condition) ) this.where[x] = fn.SINGLE(this.where[x]);
			this.where[x].type = x;
		}
	}




}

module.exports = Query;
