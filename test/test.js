var assert = require('assert');
var fs = require('fs');
const Record = require('../lib/Record');
const marc21 = require('../index.js');
const Parser = marc21.Parser;
const fn = marc21.fn;

var outDir = 'test/data/out';
var outFilePath = outDir+'/output';

var deleteFolder = function(path) {
    if(! fs.existsSync(path) ) return;
    fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) {
            deleteFolderRecursive(curPath);
        } else {
            fs.unlinkSync(curPath);
        }
    });
    fs.rmdirSync(path);
}

var fileRecords = [
    /*0*/ { id: '32062367X'  , subject : 'mars volta'},
    /*1*/ { id: '971124434'  , subject : 'radiohead'},
    /*2*/ { id: '1057018554' , subject : 'alt-j'},
    /*3*/ { id: '118508288'  , subject : 'beethoven'},
    /*4*/ { id: '118520539'  , subject : 'chopin'},
    /*5*/ { id: '118626523'  , subject : 'verdi'},
    /*6*/ { id: '118584596'  , subject : 'mozart'},
    /*7*/ { id: '1116096951' , subject : 'feininger'},
    /*8*/ { id: '98896791X'  , subject : 'iterative methods'},
]

const numRecordsInFile = fileRecords.length;


var checkIds = function(results, ids) {
    var found = {};
    for(var i=0; i<ids.length; i++) {
        found[ids[i]] = false;
    }

    for(var i=0; i<results.length; i++){
        if(ids.indexOf(results[i].get().controlfields[0].value) != -1) found[results[i].get().controlfields[0].value] = true;
    }

    for(var i=0; i<ids.length; i++) {
        if(found[ids[i]] == false ) return false
    }

    return true;
}


describe('Files', function() {

    beforeEach(function() {
        deleteFolder(outDir);
        fs.mkdirSync(outDir);
    });

    afterEach(function() {
        deleteFolder(outDir);
    });

    it('should open a plain xml file and read all records in memory ('+numRecordsInFile+')', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml', memory : true, log : false });
        parser.find().then((results) => {
            if(results.length != numRecordsInFile) done(new Error('different length'));

            for(var i=0; i<results.length; i++) {
                if(results[i].id() != fileRecords[i].id) return done(new Error('Not retrieved correctly'));
            }

            done();
        });
    });

    it('should open a gzipped xml file and read all records in memory ('+numRecordsInFile+')', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find().then((results) => {
            if(results.length != numRecordsInFile) done(new Error('different length'));

            for(var i=0; i<results.length; i++) {
                if(results[i].id() != fileRecords[i].id) return done(new Error('Not retrieved correctly'));
            }

            done();
        });
    });

    it('should create three output files', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', out : outFilePath, outFormat : ['json', 'xml', 'human'], rotate : false, log : false });
        parser.find().then(() => {
            if(!fs.existsSync(outFilePath + '.json')) {
                return done(new Error('json file not created'));
            }

            if(!fs.existsSync(outFilePath + '.xml')) {
                return done(new Error('xml file not created'));
            }

            if(!fs.existsSync(outFilePath + '.txt')) {
                return done(new Error('txt file not created'));
            }

            done();
        });
    });

    it('should write all records into a JSON file', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', out : outFilePath, outFormat : 'json', rotate : false, log : false });
        parser.find().then(() => {
            setTimeout(function(){
                if(!fs.existsSync(outFilePath + '.json')) {
                    return done(new Error('json file not created'));
                }

                try {
                    var json = fs.readFileSync(outFilePath + '.json');
                    json = JSON.parse(json);
                }catch(e) {
                    return done(new Error('json file could not be parsed'));
                }

                if(json.length != numRecordsInFile) return done(new Error('different length'));

                for(var i=0, j; i<json.length; i++) {
                    if(!json[i].controlfields || !json[i].controlfields.length) return done(new Error('Malformed'));

                    for(j=0; j<json[i].controlfields.length; j++) {
                        if(json[i].controlfields[j].tag == '001' && json[i].controlfields[j].value != fileRecords[i].id) {
                            return done(new Error('Not retrieved correctly'));
                        }
                    }
                }

                done();
            }, 10)
        });
    });

    it('should write all records into a XML file', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', out : outFilePath, outFormat : 'xml', rotate : false, log : false });
        parser.find().then(() => {
            if(!fs.existsSync(outFilePath + '.xml')) {
                return done(new Error('xml file not created'));
            }

            var parser2 = new Parser({ in : outFilePath+'.xml', memory : true, log : false })

            parser2.find().then((results) => {
                if(results.length != numRecordsInFile) return done(new Error('different length'));

                for(var i=0; i<results.length; i++) {
                    if(results[i].id() != fileRecords[i].id) return done(new Error('Not retrieved correctly'));
                }

                done();
            });
        });
    });

    it('should write one record per output file', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', out : outFilePath, outFormat : 'json', rotate : 1, log : false });
        this.timeout(3000);
        parser.find().then(() => {
            setTimeout(function(){ // wait for last file to be available
                for(var i=0, outFileName, json; i<numRecordsInFile; i++) {
                    outFileName = outFilePath;
                    if(i>0) outFileName += '.'+i;
                    outFileName += '.json';

                    if(!fs.existsSync(outFileName)) {
                        return done(new Error('json file not created'));
                    }

                    try {
                        json = fs.readFileSync(outFileName);
                        json = JSON.parse(json);
                    }catch(e) {
                        return done(new Error('json file could not be parsed'));
                    }

                    if(json.length != 1) return done(new Error('json file did not contain 1 record'));

                    for(var j=0; j<json[0].controlfields.length; j++) {
                        if(json[0].controlfields[j].tag == '001' && json[0].controlfields[j].value != fileRecords[i].id) {
                            return done(new Error('Not retrieved correctly'));
                        }
                    }
                }

                done();
            }, 10);
        });

    });

});



















describe('Query', function() {

    it('should find 1 record via value selector', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { value : '9783868323702' } }).then((results) => {
            if(results.length != 1) return done(new Error('Not found'));

            if(results[0].get().controlfields[0].value != '1116096951') return done(new Error('Wrong record found'));
            done();
        });
    });

    it('should find 1 record via value selector (EQUALS)  - case sensitive', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { value : fn.EQUALS('Burg, Tobias', 'salzburg') } }).then((results) => {
            if(results.length != 1) return done(new Error('Not found'));

            if(results[0].get().controlfields[0].value != '1116096951') return done(new Error('Wrong record found'));
            done();
        });
    });

    it('should find 1 record via value selector (CONTAINS) - case insensitive', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { value : fn.CONTAINS('mozart') } }).then((results) => {
            if(results.length != 1) return done(new Error('Not found'));

            if(results[0].get().controlfields[0].value != '118584596') return done(new Error('Wrong record found'));
            done();
        });
    });

    it('should find 4 records with multiple value selector (CONTAINS) - case insensitive', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { value : fn.CONTAINS('mozart', 'verdi', 'chopin', 'mars volta') } }).then((results) => {
            if(results.length != 4) return done(new Error('Not found'));

            if(!checkIds(results, ['118520539', '32062367X', '118626523', '118584596'])) return done(new Error('Wrong records fetched'));
            done();
        });
    });


    it('should find 1 record with multiple value selector (CONTAINS_ALL) - case insensitive', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { value : fn.CONTAINS_ALL('gunda', 'und', 'kunstpalast') } }).then((results) => {
            if(results.length != 1) return done(new Error('Not found'));

            if(!checkIds(results, ['1116096951'])) return done(new Error('Wrong records fetched'));
            done();
        });
    });

    it('should not find any record with multiple value selector (CONTAINS_ALL) - case insensitive', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { value : fn.CONTAINS_ALL('gunda', 'und', 'kunstpalast', 'mm') } }).then((results) => {
            if(results.length != 0) return done(new Error('Not found'));
            done();
        });
    });


    it('should find two records (OR)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({
            datafield : fn.OR({ value : fn.CONTAINS('mozart') },
                              { value : fn.CONTAINS('verdi') })
        }).then((results) => {
            if(results.length != 2) return done(new Error('Not found'));
            if(!checkIds(results, ['118584596', '118626523'])) return done(new Error('Wrong records fetched'));
            done();
        });
    });

    it('should not find any record (AND)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({
            datafield : fn.AND({ value : fn.CONTAINS('mozart') },
                               { value : fn.CONTAINS('verdi') })
        }).then((results) => {
            if(results.length != 0) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find one record by id', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.findById('118626523').then((results) => {
            if(results.length != 1) return done(new Error('Not found'));
            if(results[0].get().controlfields[0].value != '118626523') return done(new Error('Wrong record found'));
            done();
        });
    });

    it('should find two records by id', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.findById('118626523', '118584596').then((results) => {
            if(results.length != 2) return done(new Error('Not found'));
            if(!checkIds(results, ['118584596', '118626523'])) return done(new Error('Wrong records fetched'));
            done();
        });
    });


    it('should find 2 records of type "Bibliographic"', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ type : 'Bibliographic' }).then((results) => {
            if(results.length != 2) return done(new Error('Did not work'));

            if(!checkIds(results, ['98896791X', '1116096951'])) return done(new Error('Wrong records fetched'));
            done();
        });
    });

    it('should find '+(numRecordsInFile-2)+' records of type "Authority"', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ type : 'Authority' }).then((results) => {
            if(results.length != (numRecordsInFile-2)) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find '+numRecordsInFile+' records with a partially specified controlfield selector', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ controlfield : { tag : '005' } }).then((results) => {
            if(results.length != numRecordsInFile) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find 1 record with a fully specified controlfield selector', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ controlfield : { tag : '005', value : '20150930141334.0' } }).then((results) => {
            if(results.length != 1) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find 1 record with a fully specified datafield selector', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { tag : '110', ind1 : '2', ind2 : ' ', code : 'g' , value : 'Musikgruppe' } }).then((results) => {
            if(results.length != 1) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find 1 record with a partially specified datafield selector (no code)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { tag : '110', ind1 : '2', ind2 : ' ', value : 'Musikgruppe' } }).then((results) => {
            if(results.length != 1) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find 3 records with a partially specified datafield selector (tag and code only)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { tag : '110', code : 'a' } }).then((results) => {
            if(results.length != 3) return done(new Error('Did not work'));
            done();
        });
    });

    it('should find 3 records with a partially specified datafield selector (tag and code only)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { tag : '110', code : 'a' } }).then((results) => {
            if(results.length != 3) return done(new Error('Did not work'));
            done();
        });
    });

    it('should not find any records with a fully specified datafield selector', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ datafield : { tag : '110', ind1 : '2', ind2 : '1', code : 'g' , value : 'Musikgruppe' } }).then((results) => {
            if(results.length != 0) return done(new Error('Did not work'));
            done();
        });
    });




});







describe('Configuration', function() {
    it('should return one result as a Record object (by default)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.findById('118626523').then((results) => {
            if( !(results[0] instanceof Record) ) return done(new Error('Was not a Record object'));
            done();
        });
    });

    it('should return one result as a generic object', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, rawRecords : true, log : false });
        parser.findById('118626523').then((results) => {
            if( results[0] instanceof Record ) return done(new Error('Was not a raw object'));
            done();
        });
    });
});










describe('Limits', function() {

    it('should only retrieve 3 results starting from the first (limit 3, offset default)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ select : '*', limit : 3 }).then((results) => {
            if(results.length != 3) return done(new Error('Wrong number of results'));
            done();
        });
    });

    it('should only retrieve 2 results starting from the 4th (limit 2, offset 3)', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        parser.find({ select : '*', limit : 2, offset : 3 }).then((results) => {
            if(results.length != 2) return done(new Error('Wrong number of results'));

            if(results[0].get().controlfields[0].value != '118508288') return done(new Error('Wrong record fetched'));
            done();
        });
    });

    it('should fire the callback for each record and not save results in memory by default', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', log : false });
        var count = 0;

        parser.on('record', (record) => {
            count++;
        })

        parser.find().then((results) => {
            if(count != numRecordsInFile) return done(new Error('Not all records returned'));
            if(results.length) return done(new Error('Records were saved in memory'));
            done();
        });
    });


    it('should fire the callback for each record and save results in memory as per configuration', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', memory : true, log : false });
        var count = 0;

        parser.on('record', (record) => {
            count++;
        })

        parser.find().then((results) => {
            if(count != numRecordsInFile) return done(new Error('Not all records returned'));
            if(!results.length) return done(new Error('Records were NOT saved in memory'));
            done();
        });
    });


    it('should fire the callback every 4 records', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', callbackFrequency : 4, log : false });
        var count = 0;
        var lastNum = 0;
        var first = true;
        parser.on('record', (records) => {
            if(!first && lastNum != 4) {
                done(new Error('Passed wrong number ('+lastNum+') and was not last callback'));
                return false;
            }
            count += records.length;
            lastNum = records.length;
            first = false;
        })

        parser.find().then(() => {
            if(count != numRecordsInFile) return done(new Error('Not all records returned ('+count+'/'+numRecordsInFile+')'));
            done();
        }, () => {});
    });

    it('should abort parsing from callback', function(done) {
        var parser = new Parser({ in : 'test/data/all.xml.gz', log : false });
        var firstRecord = true;

        parser.on('record', (records) => {
            if(!firstRecord) done(new Error('Another callback was fired after'));
            firstRecord = true;
            return false;
        })

        parser.find().then(
            () => { done(new Error('Success callback was fired')); },
            (r) => { if(r) return done(new Error('Error callback was passed an argument')); done() }
        );
    });
});
